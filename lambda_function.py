import json
import boto3

def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    # request info
    scraped_info = scrape_twitter()
    BUCKET_NAME = "twitter-scraper-bucket"
    DATE = f"{scraped_info['date']}"
    OUTPUT_NAME = f"dataKeyTest{Date}.json"
    OUTPUT_BODY = json.dumps(scraped_info)
    # save data into bucket
    s3.Bucket(BUCKET_NAME).put_object(Key = OUTPUT_NAME, Body = OUTPUT_BODY)
    print(f"Job done at {DATE}")