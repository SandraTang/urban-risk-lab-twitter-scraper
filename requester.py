# INFORMATION ON THE TWITTER API
# A Twitter developer account is required to use the Twitter API. 
# Each account has a bearer token which is required to call the Twitter API. 
# Free use of the Twitter API has the following request rate limits - 10/sec, 30/min, 250/month. 
# Twitter API v1 is stable while v2 is usable but early access. 
# Free third-party Twitter scrapers were usable and not limited by request rate limits, but
# stopped working after Twitter altered its front-end roughly around September. 

# INFORMATION ON USING THIS METHOD WITH AWS
# This file is the code used in the AWS Lambda function. 
# The following is information on the parts of AWS used in conjuction with this. 
# IAM Role - created to allow the Lambda layer to access the S3 bucket
# Lambda layer - collects the data
# Lambda fucntion - asks for the data (and processed and logs)
# S3 Bucket - holds the data
# IAM Role is selected as the role while creating the Lambda layer. 

import requests
import time
import json

def fetch_tweets_from_query(query, fromDate, toDate):
  """
  About:  This method takes in a query (keywords) and a date range from Riskmap's API, scrapes Twitter with the 
          Twitter API, parses the returned tweet JSON data, logs it into Riskmap's database with the Riskmap API,
          waits for one second, and repeats the process until there are no more results to be processed from Twitter.
  Input:  (1) query - search keywords for the Twitter API, e.g. (damage OR earthquake OR flood OR (natural disaster))
          (2) fromDate, toDate - date range, each date is in the format YYYYMMDDhhmm
  Output: no return statement
  Next:   What needs to be added to this code. 
          (A) fix headers 
          (B) fix tweet parsing to select out the proper info
          (C) use the proper keys
  """
  next_token = ''
  twitter_API_endpoint = 'https://api.twitter.com/1.1/tweets/search/30day/dev.json'
  riskmap_endpoint = 'https://api.staging.riskmap.org/docs/#/message/post_message_log_bulk'
  while next_token != None: # while there are results to be scraped, logged, and processed from Twitter
    # SCRAPE FROM TWITTER API
    headers = '...'
    data = '{"query":' + query + ', "fromDate": ' + fromDate + ', "toDate": "' + toDate + '"}' #"maxResults": 10
    # (A) fix headers
    response = requests.post(twitter_API_endpoint, data=data, headers=headers).json() # get from the api endpoint
    tweets = response.results # list of tweets from the response capped at 100 tweets/request
    # PARSE TWEETS
    # (B) fix tweet parsing to select out the proper info
    parsed_tweets = []
    for tweet in tweets:
      tweet_dict = json.load(tweet) # convert JSON to python dictionary
      # construct a dictionary with the required keys from API body
      # these are not the proper keys, but left here as an example of the idea
      # (C) use the proper keys
      parsed_tweets.append({"accept": tweet_dict["accept"], "Authorization": tweet_dict["Authorization"], "Content-Type": tweet_dict["Content-Type"], "Referer": tweet_dict["Referer"]})
    # SEND PARSED TWEETS TO RISKMAP 
    # options, header, authorization, bearer, scraperirsklab --> another header referer 
    # (A) fix headers
    response = request.post(riskmap_endpoint, data = parsed_tweets, header = "bearer scraper@risklab").json()
    next_token = response.next # recieve the next token to scrape the next batch of tweets
    time.sleep(1) # wait for at least one second to make the next requests